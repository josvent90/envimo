package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DatosRecibeActivity extends AppCompatActivity {

    Button botonSiguiente;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_datos_recibe);
        botonSiguiente = findViewById(R.id.buttonDatosRecibe);
        botonSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambio();
            }
        });

    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.resumenActivty");
        startActivity(inicio);
    }
}
