package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class InicioActivity extends AppCompatActivity {

    Button continuar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio);
        continuar = findViewById(R.id.buttonInicio);
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambio();
            }
        });

    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.segundaActivty");
        startActivity(inicio);
    }
}
