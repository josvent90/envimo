package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class PagoRealizadoActivity extends AppCompatActivity {

    Button buttonContinuar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_envio_realizado);
        buttonContinuar = findViewById(R.id.buttonRealizado);
        buttonContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambio();
            }
        });
    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.inicio");
        startActivity(inicio);
    }
}
