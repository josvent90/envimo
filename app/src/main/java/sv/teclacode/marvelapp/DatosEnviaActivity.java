package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class DatosEnviaActivity extends AppCompatActivity {

    Button botonSiguiente;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_datos_envio);
        botonSiguiente = findViewById(R.id.buttonInicioEnvio);
        botonSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambio();
            }
        });
    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.tercerActivty");
        startActivity(inicio);
    }
}
