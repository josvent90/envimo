package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;

public class splashactivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_start);
        Asyncpreparar tarea = new Asyncpreparar(this);
        tarea.execute();
    }

    private class Asyncpreparar extends AsyncTask<String, String, String> {
        splashactivity s;

        public Asyncpreparar(splashactivity s) {
            this.s = s;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            s.cambio();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.inicio");
        startActivity(inicio);
    }
}
