package sv.teclacode.marvelapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ResumenActivity extends AppCompatActivity {

    Button continuarButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_resumen_transaccion);
        continuarButton = findViewById(R.id.buttonResumen);
        continuarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambio();
            }
        });
    }

    void cambio() {
        Intent inicio = new Intent("sv.teclacode.marvelapp.pagoRealizadoActivty");
        startActivity(inicio);
    }
}
